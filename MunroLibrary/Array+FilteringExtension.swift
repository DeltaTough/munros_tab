//
//  Array+FilteringExtension.swift
//  MunroLibrary
//
//  Created by Dimitrios Tsoumanis on 11/02/2021.
//  Copyright © 2021 Dimitrios Tsoumanis. All rights reserved.
//

import Foundation

extension Array where Element == MunroHill {
    func filterByHill(_ hill: String?) -> [MunroHill]? {
        let munroHills = self.filter { (munroHill) -> Bool in
            return munroHill.hillCategory == hill?.capitalized
        }
        return munroHills
    }
    
    func filterByMinHeight(_ minHeight: Double) -> [MunroHill]? {
        let munroHills = self.filter { (munroHill) -> Bool in
            return munroHill.heightInMetres < minHeight
        }
        return munroHills
    }
    
    func filterByMaxHeight(_ maxHeight: Double) -> [MunroHill]? {
        let munroHills = self.filter { (munroHill) -> Bool in
            return munroHill.heightInMetres > maxHeight
        }
        return munroHills
    }
    
    func filterByHeightRange(_ minHeight: Double, _ maxHeight: Double) -> [MunroHill]? {
        let munroHills = self.filter { (munroHill) -> Bool in
            return munroHill.heightInMetres > minHeight && munroHill.heightInMetres < maxHeight
        }
        return munroHills
    }
    
}
