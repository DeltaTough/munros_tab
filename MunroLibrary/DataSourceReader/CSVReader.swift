//
//  CSVReader.swift
//  MunroLibrary
//
//  Created by Dimitrios Tsoumanis on 08/02/2021.
//  Copyright © 2021 Dimitrios Tsoumanis. All rights reserved.
//

import Foundation

public class CSVReader {
    
    private let fileManager: FileManager

    init(fileManager: FileManager = FileManager.default) {
        self.fileManager = fileManager
    }
    
    func getData() -> String {
        var data = ""
        do {
            if let filePath = self.filePath() {
                data = try String(contentsOf: filePath, encoding: .ascii)
                print(data)
            }
        } catch {
            print(error)
        }
        return data
    }
    
    func filePath() -> URL? {
        let fileName = "munrotab"
        let directory = try? fileManager.url(for: .documentDirectory,
                                             in: .userDomainMask,
                                             appropriateFor: nil,
                                             create: true)

        if let fileURL = directory?.appendingPathComponent(fileName)
                                   .appendingPathExtension("csv") {
            return fileURL
        }
        return nil
    }
    
    func convertCSVIntoArray() -> [MunroHill] {
        var munroHills = [MunroHill]()
        var rows = self.getData().components(separatedBy: "\n")
        rows.removeFirst()
        
        for row in rows {
            guard !row.isEmpty else {
                continue
            }
            let columns = row.components(separatedBy: ",")
            if columns.count == 30 {
                let name = columns[6]
                let height = Double(columns[10]) ?? 0.0
                var category = ""
                
                for hill in columns[18 ... 28] {
                    if hill == "MUN" {
                        category = "Munro"
                        break
                    } else if hill == "TOP" {
                        category = "Top"
                        break
                    } else {
                        continue
                    }
                }
                
                let grid = columns[14]
                
                let munroHill = MunroHill(name: name,
                                          heightInMetres: height,
                                          hillCategory: category,
                                          gridReference: grid)
                munroHills.append(munroHill)
            }
        }
        return munroHills
    }
}


