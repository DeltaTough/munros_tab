//
//  MunroLibraryError.swift
//  MunroLibrary
//
//  Created by Dimitrios Tsoumanis on 12/02/2021.
//  Copyright © 2021 Dimitrios Tsoumanis. All rights reserved.
//

import Foundation

public enum MunroLibraryError: Error, LocalizedError {
    case nonNegativeInteger(number: Int)
    case nonNegativeDouble(number: Double)
    case minHeightLargerThanMaxHeight(min: Double, max: Double)
    case invalidInput
    
    public var errorDescription: String? {
        switch self {
        case .nonNegativeInteger(let number):
            return "Negatives not allowed: \(Int(number))"
        case .nonNegativeDouble(let number):
            return "Negatives not allowed: \(Double(number))"
        case .minHeightLargerThanMaxHeight(let min, let max):
            return "Min height \(Double(min)) cannot be greater than Max height \(Double(max))"
        case .invalidInput:
            return "Invalid input"
        }
    }
}
