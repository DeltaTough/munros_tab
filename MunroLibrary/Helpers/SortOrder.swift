//
//  SortOrder.swift
//  MunroLibrary
//
//  Created by Dimitrios Tsoumanis on 11/02/2021.
//  Copyright © 2021 Dimitrios Tsoumanis. All rights reserved.
//

import Foundation

enum SortOrder {
    case ascending
    case descending
}
