//
//  MunroHill.swift
//  MunroLibrary
//
//  Created by Dimitrios Tsoumanis on 10/02/2021.
//  Copyright © 2021 Dimitrios Tsoumanis. All rights reserved.
//

import Foundation

final class MunroHill: Codable, Hashable {
    var name: String
    var heightInMetres: Double
    var hillCategory: String?
    var gridReference: String
    
    init(name: String,
         heightInMetres: Double,
         hillCategory: String?,
         gridReference: String) {
        self.name = name
        self.heightInMetres = heightInMetres
        self.hillCategory = hillCategory
        self.gridReference = gridReference
    }
    
    static func == (lhs: MunroHill, rhs: MunroHill) -> Bool {
        return lhs.name == rhs.name &&
            lhs.heightInMetres == rhs.heightInMetres &&
            lhs.hillCategory == rhs.hillCategory &&
            lhs.gridReference == rhs.gridReference
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(name)
    }
}
