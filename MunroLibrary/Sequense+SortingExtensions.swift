//
//  Sequense+SortingExtensions.swift
//  MunroLibrary
//
//  Created by Dimitrios Tsoumanis on 11/02/2021.
//  Copyright © 2021 Dimitrios Tsoumanis. All rights reserved.
//

import Foundation

extension Sequence {
    func sorted<T: Comparable>(
        by keyPath: KeyPath<Element, T>,
        using comparator: (T, T) -> Bool = (<)
    ) -> [Element] {
        sorted { a, b in
            comparator(a[keyPath: keyPath], b[keyPath: keyPath])
        }
    }
}

extension Sequence {
    func limit(_ limit: Int) -> [Iterator.Element] {
        var result : [Iterator.Element] = []
        result.reserveCapacity(limit)
        var count = 0
        var it = makeIterator()
        while count < limit, let element = it.next() {
                result.append(element)
                count += 1
        }
        return result
    }
}
