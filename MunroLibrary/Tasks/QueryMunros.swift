//
//  QueryMunros.swift
//  MunroLibrary
//
//  Created by Dimitrios Tsoumanis on 10/02/2021.
//  Copyright © 2021 Dimitrios Tsoumanis. All rights reserved.
//

import Foundation

class QueryMunros {
    private var csvReader: CSVReader
    
    private var getOriginalDataArray = [MunroHill]()
    
    init(csvReader: CSVReader = CSVReader()) {
        self.csvReader = csvReader
        self.getOriginalDataArray = self.csvReader.convertCSVIntoArray()
    }
    
    public func filterByCategory(_ hill: String?) throws -> [MunroHill]? {
        guard hill == "Top" || hill == "Munro" else {
            throw MunroLibraryError.invalidInput
        }
        return getOriginalDataArray.filterByHill(hill)
    }
    
    public func sortByHeightDesc() -> [MunroHill] {
        return getOriginalDataArray.sorted(by: \.heightInMetres, using: >)
    }
    
    public func sortByHeightAsc() -> [MunroHill] {
        return getOriginalDataArray.sorted(by: \.heightInMetres, using: <)
    }
    
    public func sortByNameDesc() -> [MunroHill] {
        return getOriginalDataArray.sorted(by: \.name, using: >)
    }
    
    public func sortByNameAsc() -> [MunroHill] {
        return getOriginalDataArray.sorted(by: \.name, using: <)
    }
    
    public func limitResults(_ limit: Int) throws -> [MunroHill] {
        guard limit > 0 else {
            throw MunroLibraryError.nonNegativeInteger(number: limit)
        }
        return getOriginalDataArray.limit(limit)
    }
    
    public func maxHeightInMetres(_ maxHeight: Double) throws -> [MunroHill]  {
        try self.assertNoNegativesOrThrow(maxHeight)
        return getOriginalDataArray.filterByMaxHeight(maxHeight) ?? []
    }
    
    public func minHeightInMetres(_ minHeight: Double) throws -> [MunroHill]? {
        try self.assertNoNegativesOrThrow(minHeight)
        return getOriginalDataArray.filterByMinHeight(minHeight)
    }
    
    public func heightInRange(_ minHeight: Double, _ maxHeight: Double) throws -> [MunroHill]? {
        try self.assertNoNegativesOrThrow(maxHeight)
        try self.assertNoNegativesOrThrow(minHeight)
        guard maxHeight > minHeight else {
            throw MunroLibraryError.minHeightLargerThanMaxHeight(min: minHeight, max: maxHeight)
        }
        return getOriginalDataArray.filterByHeightRange(minHeight, maxHeight)
    }
    
    public func sortByHeightAndName(_ order: SortOrder) -> [MunroHill] {
        var munros = getOriginalDataArray
        if order == .ascending {
           munros.sort {
              ($0.heightInMetres, $0.name) <
                ($1.heightInMetres, $1.name)
            }
        } else if order == .descending {
             munros.sort {
              ($1.heightInMetres, $1.name) <
                ($0.heightInMetres, $0.name)
            }
        }
        return munros
    }
    
    private func assertNoNegativesOrThrow(_ value: Double) throws {
        guard value > 0 else {
            throw MunroLibraryError.nonNegativeDouble(number: value)
        }
    }
}
