//
//  CSVReaderTests.swift
//  MunroLibraryTests
//
//  Created by Dimitrios Tsoumanis on 08/02/2021.
//  Copyright © 2021 Dimitrios Tsoumanis. All rights reserved.
//

import XCTest
@testable import MunroLibrary

class CSVReaderTests: XCTestCase {
    
    var csvreader: CSVReader!
    
    override func setUp() {
      csvreader = CSVReader()
    }

    override func tearDown() {
      csvreader = nil
    }

    func testFilePathExists() {
        let filePath = csvreader.filePath()
        XCTAssertNotNil(filePath)
    }
    
    func testCSVReturnsData() {
        let readerData = csvreader.getData()
        XCTAssertNotEqual(readerData.isEmpty, "".isEmpty)
    }
    
    func testResultsAreNotNull() {
        XCTAssertNotNil(csvreader.convertCSVIntoArray())
    }
    
    func testResultsHaveAtLeastOneValue() {
        XCTAssertGreaterThan(csvreader.convertCSVIntoArray().count, 0)
    }
    
    func testConvertCSVIntoArray() {
        let munroHillsArray = csvreader.convertCSVIntoArray()
        let munroHill1 = MunroHill(name: "Ben Chonzie",
                                  heightInMetres: 931,
                                  hillCategory: "Munro",
                                  gridReference: "NN773308")
        let munroHill2 = MunroHill(name: "An Stuc",
                                   heightInMetres: 1117.1,
                                   hillCategory: "Top",
                                   gridReference: "NN638431")
        let munroHill3 = MunroHill(name: "Sgurr Ban",
                                   heightInMetres: 989,
                                   hillCategory: "Munro",
                                   gridReference: "NH055745")
        let munroHills = [munroHill1, munroHill2, munroHill3]
        let munroHillsSet: Set = Set(munroHills)
        let munroHillsArraySet: Set = Set(munroHillsArray)
        let containsAll = munroHillsSet.isSubset(of: munroHillsArraySet)
        XCTAssertTrue(containsAll)
    }
}
