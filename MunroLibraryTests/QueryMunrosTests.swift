//
//  QueryMunrosTests.swift
//  MunroLibraryTests
//
//  Created by Dimitrios Tsoumanis on 10/02/2021.
//  Copyright © 2021 Dimitrios Tsoumanis. All rights reserved.
//

import XCTest
@testable import MunroLibrary

class QueryMunrosTests: XCTestCase {
    
    var queryMunros: QueryMunros!
    
    override func setUp() {
        queryMunros = QueryMunros()
    }
    
    override func tearDown() {
        queryMunros = nil
    }
    
    private func arrayOfMunros() -> [MunroHill] {
        let munroHill1 = MunroHill(name: "Ben Chonzie",
                                   heightInMetres: 931,
                                   hillCategory: "Munro",
                                   gridReference: "NN773308")
        let munroHill2 = MunroHill(name: "An Stuc",
                                   heightInMetres: 1117.1,
                                   hillCategory: "Top",
                                   gridReference: "NN638431")
        let munroHill3 = MunroHill(name: "Sgurr Ban",
                                   heightInMetres: 989,
                                   hillCategory: "Munro",
                                   gridReference: "NH055745")
        let munroHills = [munroHill1, munroHill2, munroHill3]
        return munroHills
    }
    
    func testFilterByCategory() {
        let munroHill1 = MunroHill(name: "Ben Chonzie",
                                   heightInMetres: 931,
                                   hillCategory: "Munro",
                                   gridReference: "NN773308")
        let munroHill2 = MunroHill(name: "Sgurr Ban",
                                   heightInMetres: 989,
                                   hillCategory: "Munro",
                                   gridReference: "NH055745")
        let _munroHills = [munroHill1, munroHill2]
        let munroHills = arrayOfMunros().filterByHill("munro")
        XCTAssertEqual(munroHills, _munroHills)
    }
    
    func testFilterByCategoryWithNoHill() {
        let munroHill1 = MunroHill(name: "Ben Chonzie",
                                   heightInMetres: 931,
                                   hillCategory: "Munro",
                                   gridReference: "NN773308")
        let munroHill2 = MunroHill(name: "Sgurr Ban",
                                   heightInMetres: 989,
                                   hillCategory: "Munro",
                                   gridReference: "NH055745")
        let munroHill3 = MunroHill(name: "An Stuc",
                                   heightInMetres: 1117.1,
                                   hillCategory: "",
                                   gridReference: "NN638431")
        let _munroHills = [munroHill1, munroHill2, munroHill3]
        let munroHills = arrayOfMunros().filterByHill("munro")
        XCTAssertEqual(munroHills, _munroHills.limit(2))
    }
    
    func testHeightSortingDesc() {
        let munroHills =  arrayOfMunros().sorted(by: \.heightInMetres, using: >)
        XCTAssertEqual([1117.1, 989, 931], munroHills.map {$0.heightInMetres})
    }
    
    func testHeightSortingAsc() {
        let munroHills =  arrayOfMunros().sorted(by: \.heightInMetres, using: <)
        XCTAssertEqual([931, 989, 1117.1], munroHills.map {$0.heightInMetres})
    }
    
    func testNameSortingDesc() {
        let munroHills =  arrayOfMunros().sorted(by: \.name, using: >)
        XCTAssertEqual(["Sgurr Ban", "Ben Chonzie", "An Stuc"], munroHills.map {$0.name})
    }
    
    func testNameSortingAsc() {
        let munroHills =  arrayOfMunros().sorted(by: \.name, using: <)
        XCTAssertEqual(["An Stuc", "Ben Chonzie", "Sgurr Ban"], munroHills.map {$0.name})
    }
    
    func testLimitResults() {
        let munroHill1 = MunroHill(name: "Ben Chonzie",
                                   heightInMetres: 931,
                                   hillCategory: "Munro",
                                   gridReference: "NN773308")
        let munroHill2 = MunroHill(name: "An Stuc",
                                   heightInMetres: 1117.1,
                                   hillCategory: "Top",
                                   gridReference: "NN638431")
        let _munroHills = [munroHill1, munroHill2]
        let munroHills = arrayOfMunros().limit(2)
        XCTAssertEqual(munroHills, _munroHills)
    }
    
    func testMaxHeightInMetres() {
        let munroHills = arrayOfMunros().filterByMaxHeight(1000)
        XCTAssertEqual([1117.1], munroHills?.map {$0.heightInMetres})
    }
    
    func testMinHeightInMetres() {
        let munroHills = arrayOfMunros().filterByMinHeight(950)
        XCTAssertEqual([931], munroHills?.map {$0.heightInMetres})
    }
    
    func testHeightInRange() {
        let munroHills = arrayOfMunros().filterByHeightRange(900, 1000)
        XCTAssertEqual([931, 989], munroHills?.map {$0.heightInMetres})
    }
    
    func testFilterByCategoryShouldThrowInvalidInputError() {
        XCTAssertThrowsError(try queryMunros.filterByCategory("Munrossss")) { (error) in
            guard case MunroLibraryError.invalidInput = error else {
                return XCTFail("MunroHills does not throw an error for invalid input")
            }
            
            XCTAssertEqual(error.localizedDescription, "Invalid input")
        }
    }
    
    func testLimitShouldThrowNonNegativeIntegerError() {
        XCTAssertThrowsError(try queryMunros.limitResults(-3)) { (error) in
            guard case MunroLibraryError.nonNegativeInteger(number: -3) = error else {
                return XCTFail("MunroHills does not throw an error for negative numbers")
            }
            XCTAssertEqual(error.localizedDescription, "Negatives not allowed: -3")
        }
    }
    
    func testMaxHeightShouldThrowNonNegativeDoubleError() {
        XCTAssertThrowsError(try queryMunros.maxHeightInMetres(-10.0)) { (error) in
            guard case MunroLibraryError.nonNegativeDouble(number: -10.0) = error else {
                return XCTFail("MunroHills does not throw an error for negative numbers")
            }
            XCTAssertEqual(error.localizedDescription, "Negatives not allowed: -10.0")
        }
    }
    
    func testMinHeightShouldThrowNonNegativeDoubleError() {
        XCTAssertThrowsError(try queryMunros.minHeightInMetres(-10.0)) { (error) in
            guard case MunroLibraryError.nonNegativeDouble(number: -10.0) = error else {
                return XCTFail("MunroHills does not throw an error for negative numbers")
            }
            XCTAssertEqual(error.localizedDescription, "Negatives not allowed: -10.0")
        }
    }
    
    func testHeightInRangeShouldThrowNonNegativeDoubleError() {
        XCTAssertThrowsError(try queryMunros.heightInRange(-10.0, -10.0)) { (error) in
            guard case MunroLibraryError.nonNegativeDouble(number: -10.0) = error else {
                return XCTFail("MunroHills does not throw an error for negative numbers")
            }
            XCTAssertEqual(error.localizedDescription, "Negatives not allowed: -10.0")
        }
    }
    
    func testHeightInRangeShouldThrowInvalidInputError() {
        XCTAssertThrowsError(try queryMunros.heightInRange(1000.0, 900.0)) { (error) in
            guard case MunroLibraryError.minHeightLargerThanMaxHeight(min: 1000.0, max: 900.0) = error else {
                return XCTFail("MunroHills does not throw an error for mismatch in values")
            }
            XCTAssertEqual(error.localizedDescription,
                           "Min height 1000.0 cannot be greater than Max height 900.0")
        }
    }
    
    func testByHeightAndNameAsc() {
        var munros = arrayOfMunros()
        let munroHill4 = MunroHill(name: "Meall Buidhe",
                                   heightInMetres: 989,
                                   hillCategory: "Munro",
                                   gridReference: "NN498499")
        
        munros.append(munroHill4)
        munros.sort {
            ($0.heightInMetres, $0.name) <
                ($1.heightInMetres, $1.name)
        }
        XCTAssertEqual(["Ben Chonzie", "Meall Buidhe", "Sgurr Ban", "An Stuc"], munros.map {$0.name})
    }
    
    func testByHeightAndNameDesc() {
        var munros = arrayOfMunros()
        let munroHill4 = MunroHill(name: "Meall Buidhe",
                                   heightInMetres: 989,
                                   hillCategory: "Munro",
                                   gridReference: "NN498499")
        
        munros.append(munroHill4)
        munros.sort {
            ($1.heightInMetres, $1.name) <
                ($0.heightInMetres, $0.name)
        }
        XCTAssertEqual(["An Stuc", "Sgurr Ban", "Meall Buidhe", "Ben Chonzie"], munros.map {$0.name})
    }
}
